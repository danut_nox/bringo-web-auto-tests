from selenium import webdriver
import unittest
import time


class userGoogleLogin(unittest.TestCase):

    def setUp(self):
        self.profile = webdriver.FirefoxProfile()
        self.profile.accept_untrusted_certs = True
        self.profile.accept_ssl_certs = True
        self.driver = webdriver.Firefox(firefox_profile=self.profile)
        self.driver.set_window_size(height=1920, width=1080)
        self.mail = 'Gigiuc1748@gmail.com'
        self.parola = 'qwertyuiop1@!!'

    def testLogin(self):
        self.driver.get('http://be.ascendnet.ro')
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div[2]/span[2]/a').click()
        if 'http://be.ascendnet.ro/ro/login' == self.driver.current_url:
            print("link login corect")
        else:
            raise Exception("Link-ul de la login e gresit")

        self.driver.find_element_by_xpath('/html/body/div/div/div/div/div[2]/form/div[4]/div[2]/a').click()
        time.sleep(3)
        if 'https://accounts.google.com/signin/oauth/identifier?client_id=' in self.driver.current_url \
            and 'destination=http%3A%2F%2Fbe.ascendnet.ro&approval_state=' in self.driver.current_url \
            and 'flowName=GeneralOAuthFlow' in self.driver.current_url:
            print("Redirect success")
        else:
            raise Exception('Nu merge login cu google')

        self.driver.find_element_by_xpath('//*[@id="identifierId"]').send_keys(self.mail)
        self.driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/content/span').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[1]/div/form/content/section/div/content/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input').send_keys(self.parola)
        self.driver.find_element_by_xpath('/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/content/span').click()
        time.sleep(3)
        if 'http://be.ascendnet.ro/ro/account/profile/edit' == self.driver.current_url:
            return True
        else:
            raise Exception("Ceva nu e in regula dupa login")

    def tearDown(self):
        print("Totul a fost in regula")
        self.driver.close()


if __name__ == '__main__':
    unittest.main()