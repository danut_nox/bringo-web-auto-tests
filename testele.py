from Login import creare_cont_negativ, creare_cont_positiv, google_login, user_login, user_login_fail, login_flow_comanda, creare_export_sterge_cont
from selenium import webdriver
import unittest
import time
from Login.user_login import userLogin
from Login.creare_cont_positiv import CreateAccount
from Login.creare_cont_negativ import NegativeAccountCreation
from Login.google_login import userGoogleLogin
from Login.user_login import userLogin
from Login.user_login_fail import userLoginFail
from Login.login_flow_comanda import flow_comanda
from Login.creare_export_sterge_cont import CreareExportSterge


class teste(unittest.TestCase):

    def test_2(self):
        creare_cont_negativ.NegativeAccountCreation()

    def test_3(self):
        google_login.userGoogleLogin()

    def test_5(self):
        user_login_fail.userLoginFail()

    def test_6(self):
        login_flow_comanda.flow_comanda()

    def test_7(self):
        creare_export_sterge_cont.CreareExportSterge()


if __name__ == '__main__':
    unittest.main()