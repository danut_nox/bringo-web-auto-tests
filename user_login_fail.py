from selenium import webdriver
import unittest
import time


class userLoginFail(unittest.TestCase):

    def setUp(self):
        self.profile = webdriver.FirefoxProfile()
        self.profile.accept_untrusted_certs = True
        self.profile.accept_ssl_certs = True
        self.driver = webdriver.Firefox(firefox_profile=self.profile)
        self.driver.set_window_size(height=1920, width=1080)
        self.mail = 'cevacarenuexista@mail.com'
        self.parola = 'parolaincorecta'

    def testLogin(self):
        self.driver.get('http://be.ascendnet.ro')
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div[2]/span[2]/a').click()
        if 'http://be.ascendnet.ro/ro/login' == self.driver.current_url:
            print("link login corect")
        else:
            raise Exception("Link-ul de la login e gresit")

        self.driver.find_element_by_xpath('//*[@id="_username"]').send_keys(self.mail)
        self.driver.find_element_by_xpath('//*[@id="_password"]').send_keys(self.parola)

        self.driver.find_element_by_xpath('/html/body/div/div/div/div/div[2]/form/button').click() # Login

        if 'Date de autentificare invalide.' in self.driver.page_source:
            print("Corect")
        else:
            raise Exception("Nu a afisat mesaj de date invalide.")

    def tearDown(self):
        print("Totul a fost in regula")
        self.driver.close()


if __name__ == '__main__':
    unittest.main()