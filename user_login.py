from selenium import webdriver
import unittest
import time


class userLogin(unittest.TestCase):

    def setUp(self):
        self.profile = webdriver.FirefoxProfile()
        self.profile.accept_untrusted_certs = True
        self.profile.accept_ssl_certs = True
        self.driver = webdriver.Firefox(firefox_profile=self.profile)
        self.driver.set_window_size(height=1920, width=1080)
        self.mail = 'testautomat@mail.com'
        self.parola = 'Dan1231231'

    def testLogin(self):
        self.driver.get('http://be.ascendnet.ro')
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div[2]/span[2]/a').click()
        if 'http://be.ascendnet.ro/ro/login' == self.driver.current_url:
            print("link login corect")
        else:
            raise Exception("Link-ul de la login e gresit")

        # Username
        self.driver.find_element_by_xpath('//*[@id="_username"]').send_keys(self.mail)

        # Parola
        self.driver.find_element_by_xpath('//*[@id="_password"]').send_keys(self.parola)

        # Login
        self.driver.find_element_by_xpath('/html/body/div/div/div/div/div[2]/form/button').click()

        time.sleep(2)
        if 'http://be.ascendnet.ro/ro/account/profile/edit' == self.driver.current_url:
            return True
        else:
            raise Exception("Ceva nu e in regula dupa login")

    def tearDown(self):
        print("Totul a fost in regula")
        self.driver.close()


if __name__ == '__main__':
    unittest.main()